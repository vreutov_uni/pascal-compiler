#pragma once
#include <vector>
#include <string>
#include <algorithm>
#include <boost\algorithm\string.hpp>

#include "symbol.h"

using namespace std;

class Identifier
{
protected:
	string m_name;

public:
	Identifier(string name);

	virtual ~Identifier() { }

	string GetName() const;

	template<typename T>
	T* As();

	template<typename T>
	bool Is();
};

template<typename T>
inline T * Identifier::As() { return dynamic_cast<T*>(this); }

template<typename T>
inline bool Identifier::Is() { return As<T>() != nullptr; }

class Type : public Identifier
{
public:
	Type(string name);

	virtual Type* OperationAllowed(Key operation, Type* operand) = 0;
	static Type* GetUnderlyingType(Type* type);

protected:
	bool IsEqualOperation(Key operation);
	bool IsCompareOperation(Key operation);
	bool IsArithmeticOperation(Key operation);
};

class ScalarType : public Type
{
public:
	ScalarType(string name);
};

class RealType : public ScalarType
{
public:
	RealType();

	virtual Type* OperationAllowed(Key operation, Type* operand) override;
};

class IntType : public ScalarType
{
public:
	IntType();

	virtual Type* OperationAllowed(Key operation, Type* operand) override;
};

class StringType : public ScalarType
{
public:
	StringType();

	virtual Type* OperationAllowed(Key operation, Type* operand) override;
};

class CharType : public ScalarType
{
public:
	CharType();

	virtual Type* OperationAllowed(Key operation, Type* operand) override;
};

class EnumValue;

class EnumType : public Type
{
	vector<EnumValue*> m_values;

public:
	EnumType(string name) :
		Type(name)
	{
	}

	virtual void AddValue(string name);
	vector<EnumValue*> GetValues();

	virtual Type* OperationAllowed(Key operation, Type* operand) override;
};

class EnumValue : public Identifier
{
	EnumType* m_type;

public:
	EnumValue(string name, EnumType* type);

	EnumType * GetType();
};

class BooleanType : public EnumType
{
public:
	BooleanType() :
		EnumType("boolean")
	{
		AddValue("true");
		AddValue("false");
	}
};

class LinkedType : public Type
{
protected:
	Type* m_base_type;

public:
	LinkedType(string name, Type* base_type);
	Type * GetBaseType();
};

class AliasType : public LinkedType
{
public:
	AliasType(string name, Type* base_type);

	virtual Type* OperationAllowed(Key operation, Type* operand) override;
};

class ReferenceType : public LinkedType
{
public:
	ReferenceType(string name, Type* base_type);
	ReferenceType(Type* base_type);

	virtual Type* OperationAllowed(Key operation, Type* operand) override;
};

class Variable : public Identifier
{
	Type* m_type;

public:
	Variable(string name, Type* type);

	Type* GetType();
};

class Constant : public Variable
{
public:
	Constant(string name, Type* type);
};

enum PassingMode {
	ByValue,
	ByReference
};

class Parameter : public Variable
{
	PassingMode m_passingMode;

public:
	Parameter(string name, Type* type, PassingMode mode);

	PassingMode GetPassingMode() { return m_passingMode; }
};

class Callable : public Identifier
{
protected:
	vector<unique_ptr<Parameter>> m_parameters;

public:
	Callable(string name);

	void AddParameter(string name, Type* type, PassingMode mode);
	vector<Parameter*> GetParameters();
};

class Function : public Callable
{
	Type* m_type;

public:
	Function(string name);

	void SetType(Type* type);
	Type* GetType();
};

class Procedure : public Callable
{
public:
	Procedure(string name);
};

class Scope
{
	Scope* m_parentScope;
	Function* m_scopeFunction;

	unordered_map<string, unique_ptr<Identifier>> m_identifiers;
	vector<unique_ptr<Identifier>> m_anonymIdentifiers;

	static Scope* m_baseScope;

public:
	Scope(Scope* parentScope = nullptr, Function* parentFunction = nullptr);

	~Scope()
	{
	}

	Scope* GetParentScope() const;
	Function* GetScopeFunction() const;

	Identifier* FindIdentifier(string name, bool recursive = true) const;
	void AddIdentifier(Identifier* ident);

	static Scope* GetBaseScope();
	
	BooleanType* GetBooleanType() const;

	template<typename T>
	static T* GetBaseType();
};

template<class T>
inline T * Scope::GetBaseType()
{
	string typeName;

	static_assert(is_same<T, IntType>::value
		|| is_same<T, RealType>::value
		|| is_same<T, CharType>::value
		|| is_same<T, StringType>::value, "Unknown type");

	if (is_same<T, IntType>::value) typeName = "integer";
	else if (is_same<T, RealType>::value) typeName = "real";
	else if (is_same<T, CharType>::value) typeName = "char";
	else if (is_same<T, StringType>::value) typeName = "string";

	return GetBaseScope()->FindIdentifier(typeName)->As<T>();
}

struct ExpressionResult
{
	Type* ResultType;
	bool IsVariable;

	ExpressionResult();
};
