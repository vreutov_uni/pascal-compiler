// main.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "compiler.h"
#include "lexer.h"

#include <iostream>
#include <fstream>
#include <string>

#include <experimental/filesystem> // C++-standard header file name  
#include <filesystem> // Microsoft-specific implementation header file name  
namespace fs = std::experimental::filesystem::v1;

void usage(const string& program_name)
{
	cout << "Usage: " << program_name << " (<path to file> | <path to test folder>) [<check errors>]" << endl;
	cout << "<check errors>" << "= e | ne | n" << endl;
	cout << "    e - check that all tests fail to compile" << endl;
	cout << "    ne - check that all tests compile successfully" << endl;
	cout << "    n - don't check tests (default)" << endl;
}

int main(int argc, char *argv[]) {
	string program_name = argv[0];
	
	if (argc < 2) {
		
		usage(program_name);
		return 1;
	}

	string input = argv[1];
	string checkArg = argc > 2 ? argv[2] : "n";

	bool checkErrors = checkArg.compare("n") != 0;
	bool checkPositive = checkArg.compare("ne") == 0;
	int countAll = 0;
	int countPassed = 0;

	setlocale(LC_ALL, "Russian");

	auto lexer = std::make_shared<Lexer>();
	Compiler c(lexer);

	if (fs::is_directory(input))
	{
		string name_log = "out/output.txt";
		ofstream fout;
		fout.open(name_log, ios::out);

		lexer->SetOutputStream(&fout);

		for (auto & p : fs::directory_iterator(input)) {
			if (fs::is_directory(p))
				continue;

			ifstream fin(p);
			std::cout << p << endl;

			fout << p << endl;
			c.Compile(fin);

			if (checkErrors)
			{
				++countAll;

				if (checkPositive != lexer->HasErrors())
				{
					std::cout << ">>> Test passed" << std::endl;
					++countPassed;
				}
				else
					std::cout << ">>> Test failed" << std::endl;
			}
			else
				std::cout << ">>> Finished" << std::endl;
		}

		if (checkErrors)
		{
			std::cout << "Ran " << countAll << " tests: " << countPassed << " passed." << std::endl;
		}
	}
	else
	{
		lexer->SetOutputStream(&std::cout);
		c.Compile(input);

		ofstream out("out/bytes.bin", std::ios::out | std::ios::binary);
		if (c.GetBytes(out))
			cout << "Compiled succesfully" << endl;
		else
			cout << "Compilation failed" << endl;
	}

	return 0;
}