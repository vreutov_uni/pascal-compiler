﻿#include "stdafx.h"
#include "compiler.h"

#include <fstream>
#include <sstream>

class unexpected_eof : std::runtime_error
{
public:
	unexpected_eof() :
		std::runtime_error("Unexpected EOF reached")
	{
	}
};

class syntax_error : std::runtime_error
{
public:
	syntax_error() :
		std::runtime_error("Syntax error")
	{
	}
};

class semantic_error : std::runtime_error
{
public:
	semantic_error() :
		std::runtime_error("Semantic error")
	{
	}
};

map<Starters, set<Key>> Compiler::m_starters_map = {
	{ EmptyStarters, { } },
	{ BlockStarters, { Key::Begin, Key::Const, Key::Type, Key::Var, Key::Function } },
	// TODO
};

Compiler::Compiler(std::shared_ptr<ILexer> lexer) :
	m_lexer(lexer)
{
}

Compiler::~Compiler()
{
}

void Compiler::Compile(const std::string & program)
{
	std::ifstream file(program);
	Compile(file);
}

void Compiler::Compile(istream & program_stream)
{
	m_skippedToEnd = false;
	m_lexer->SetInputStream(&program_stream);
	m_generator.ClearBytes();
	m_generator.SetEnabled(true);

	InitializeBaseScope();

	NextSymbol();
	try
	{
		Parse(&Compiler::Program, { Key::Program }, { Key::EndOfFile }, true, ExpectedProgram);
	}
	catch (const unexpected_eof&) {
		if (!m_skippedToEnd)
			RaiseSymbolError(GetCodeFor(Key::EndOfFile));
	}
	catch (const std::exception& ex) {
		string error = "Неизвестная ошибка: " + string(ex.what());
		m_lexer->Print(error);
	}

	CloseAllScopes();

	if (m_lexer->HasErrors())
		m_lexer->ListErrors();
	else
		m_lexer->Print(
			"Program is fine");
}

void Compiler::Parse(void (Compiler::* callable)(const set<Key>&), Starters starters_map_key, const set<Key>& followers, bool raiseError, ErrorCode errorCode)
{
	Parse(callable, m_starters_map[starters_map_key], followers, errorCode);
}

bool Compiler::CheckStarters(const std::set<Key> & starters, bool raiseError, ErrorCode errorCode, const std::set<Key> & followers)
{
	if (starters.size() > 0 && starters.find(m_currentSymbol->GetKeyword()) == starters.end())
	{
		if (!raiseError) return false;

		RaiseSymbolError(errorCode);
		SkipTo(starters + followers);
	}

	return true;
}

bool Compiler::CheckFollowers(const std::set<Key> & followers, bool wasError)
{
	if (followers.find(m_currentSymbol->GetKeyword()) == followers.end() && !m_skippedToEnd)
	{
		if (!wasError)
		{
			if (followers.find(Key::Semicolon) != followers.end())
				return false;
			else
			{
				if (followers.size() == 1)
					RaiseSymbolError(GetCodeFor(*followers.begin()));
				else
					RaiseSymbolError(ForbiddenSymbol);
			}

		}

		SkipTo(followers);
	}

	return true;
}

void Compiler::Parse(
	// указатель на функцию анализа конструкции
	void (Compiler::* callable)(const set<Key>&),
	// множество символов, с которых может
	// начинаться конструкция
	const set<Key>& starters,
	// множество символов, которые должны идти
	// непосредственно после конструкции
	const set<Key>& followers,
	// флаг, устанавливающий, нужно ли формировать ошибки
	bool raiseError,
	// код ошибки, который используется,
	// если текущий символ не принадлежит starters
	ErrorCode errorCode)
{
	// проверить принадлежность символа множеству starters
	// в случае ошибки пропустить символы до любого символа из starters или followers
	if (!CheckStarters(starters, raiseError, errorCode, followers)) return;

	// проверить принадлежность символа множеству starters
	if (starters.size() == 0 || starters.find(m_currentSymbol->GetKeyword()) != starters.end())
	{
		bool wasError = false;

		try
		{
			// запустить анализ конструкции
			(this->*callable)(followers);
		}
		catch (const syntax_error&) { wasError = true; }
		catch (const semantic_error&) { wasError = true; }

		// проверить принадлежность символа множеству followers
		// в случае ошибки пропустить символы до любого символа из followers
		if (!CheckFollowers(followers, wasError)) return;
	}
}

bool Compiler::GetBytes(ostream& output)
{
	bool success = true;

	if (!m_generator.GetEnabled() || m_lexer->HasErrors())
	{
		m_generator.ClearBytes();
		m_generator.PutByte(g::ByteCode::Begin);
		m_generator.PutByte(g::ByteCode::End);

		success = false;
	}

	auto bytes = m_generator.GetBytes();

	for (auto byte : bytes)
		output << byte;

	return success;
}

Symbol* Compiler::NextSymbol()
{
	m_currentSymbol = m_lexer->GetNextSymbol();
	return m_currentSymbol.get();
}

void Compiler::Program(const set<Key>& followers)
{
	OpenScope();

	Accept(Key::Program);
	Accept(Key::Identifier);

	m_generator.PutByte(g::ByteCode::Begin);
	
	if (TryAccept(Key::OpenRoundBracket))
	{
		Accept(Key::Identifier);

		while (TryAccept(Key::Comma))
		{
			Accept(Key::Identifier);
		}

		Accept(Key::CloseRoundBracket);
	}
	
	Accept(Key::Semicolon);

	Parse(&Compiler::Block, BlockStarters, { Key::Dot });

	Accept(Key::Dot);

	m_generator.PutByte(g::ByteCode::End);
	CloseScope();
}

void Compiler::Block(const set<Key>& followers)
{
	auto inblock_followers = unite(followers,
		{ Key::Var, Key::Function, Key::Begin});

	Parse(&Compiler::BlockTypes, { Key::Type }, inblock_followers, false);

	inblock_followers.erase(Key::Var);
	Parse(&Compiler::BlockVariables, { Key::Var }, inblock_followers, false);
	
	inblock_followers.erase(Key::Function);
	Parse(&Compiler::BlockFunctions, { Key::Function }, inblock_followers, false);
	
	inblock_followers.erase(Key::Begin);
	Parse(&Compiler::ParseBlockStatements, { Key::Begin }, inblock_followers);
}

void Compiler::BlockContants(const set<Key>& followers)
{
	if (TryAccept(Key::Const))
	{
		do
		{
			Parse(&Compiler::ConstantDefinition, { Key::Identifier }, unite(followers, Key::Semicolon));
			Accept(Key::Semicolon);
		} while (TryAccept(Key::Identifier, false));
	}
}

void Compiler::ConstantDefinition(const set<Key>& followers)
{
	Accept(Key::Identifier);
	string name = m_acceptedSymbol->GetString();

	Accept(Key::Equal);
	Type* type = ParseConstant();
	m_scope->AddIdentifier(new Constant(name, type));
}

Type* Compiler::ParseConstant()
{
	bool has_sign = (TryAccept(Key::Add) || TryAccept(Key::Subtract));
	Symbol maybeSign = Symbol(*m_acceptedSymbol);

	Type* type = nullptr;

	if (TryAccept(Key::Identifier))
	{
		auto ident = GetAcceptedIdentifier();

		if (ident == nullptr)
			RaiseAcceptedSymbolError(IdentifierNotDefined);
		else if (auto constIdent = ident->As<Constant>())
			type = constIdent->GetType();
		else
			RaiseAcceptedSymbolError(InvalidIdentifierUse);
	}
	else if (TryAccept(Key::Constant))
	{
		type = GetConstantType(dynamic_cast<ValueSymbol*>(m_acceptedSymbol.get()));
	}
	else
	{
		RaiseSymbolError(InvalidConstant, true);
	}

	if (has_sign && type && (type->Is<CharType>() || type->Is<StringType>()))
		InternalRaiseError(ForbiddenSymbol, &maybeSign);

	return type;
}

void Compiler::TypeDefinition(const set<Key>& followers)
{
	Accept(Key::Identifier);
	string name = m_acceptedSymbol->GetString();

	if (GetAcceptedIdentifier(false))
	{
		RaiseAcceptedSymbolError(DuplicateDefinition, true);
	}

	Accept(Key::Equal);
	Type* type = ParseType();
	m_scope->AddIdentifier(new AliasType(name, type));
}

Type* Compiler::ParseType()
{
	bool is_reference = TryAccept(Key::Caret);

	if (Type* type = AcceptType())
	{
		if (is_reference)
		{
			Type* ref_type = new ReferenceType(type);
			m_scope->AddIdentifier(ref_type);
			return ref_type;
		}
		else
			return type;
	}

	return nullptr;
}

void Compiler::BlockTypes(const set<Key>& followers)
{
	if (TryAccept(Key::Type))
	{
		do
		{
			Parse(&Compiler::TypeDefinition, { Key::Identifier }, unite(followers, Key::Semicolon));
			Accept(Key::Semicolon);
		} while (TryAccept(Key::Identifier, false));
	}
}

void Compiler::BlockVariables(const set<Key>& followers)
{
	if (TryAccept(Key::Var))
	{
		do
		{
			Parse(&Compiler::SameTypeVariablesDefinition, { Key::Identifier }, unite(followers, Key::Semicolon));
			Accept(Key::Semicolon);

		} while (TryAccept(Key::Identifier, false));
	}
}

void Compiler::SameTypeVariablesDefinition(const set<Key>& followers)
{
	vector<string> names;

	do
	{
		Accept(Key::Identifier);

		if (GetAcceptedIdentifier(false) || find(names.begin(), names.end(), m_acceptedSymbol->GetString()) != names.end())
		{
			RaiseAcceptedSymbolError(DuplicateDefinition);
		}
		else
		{
			names.push_back(m_acceptedSymbol->GetString());
		}

	} while (TryAccept(Key::Comma));

	Accept(Key::Colon);

	Type* type = ParseType();
	for (auto& name : names)
	{
		m_scope->AddIdentifier(new Variable(name, type));
	}
}

void Compiler::BlockFunctions(const set<Key>& followers)
{
	// TODO
	while (TryAccept(Key::Function, false))
	{
		Parse(&Compiler::FunctionDefinition, { Key::Function }, unite(followers, Key::Semicolon));
		Accept(Key::Semicolon);
	}
}

void Compiler::FunctionDefinition(const set<Key>& followers)
{
	Function* function = ParseFunctionHeader();
	OpenScope(function);
	Parse(&Compiler::Block, BlockStarters, unite(followers, Key::Semicolon));
	CloseScope();
}

Function* Compiler::ParseFunctionHeader()
{
	Accept(Key::Function);

	Accept(Key::Identifier);

	if (GetAcceptedIdentifier(false))
	{
		RaiseAcceptedSymbolError(DuplicateDefinition);
	}

	Function* function = new Function(m_acceptedSymbol->GetString());

	Accept(Key::OpenRoundBracket);

	do
	{
		ParseFormalParametersSection(function);
	} while (TryAccept(Key::Semicolon));

	Accept(Key::CloseRoundBracket);
	Accept(Key::Colon);

	Type* type = AcceptType();

	function->SetType(type);
	m_scope->AddIdentifier(function);

	Accept(Key::Semicolon);

	return function;
}

void Compiler::ParseFormalParametersSection(Function* function)
{
	bool byRef = TryAccept(Key::Var);
	
	vector<string> names;

	do
	{
		Accept(Key::Identifier);
		names.push_back(m_acceptedSymbol->GetString());

	} while (TryAccept(Key::Comma));

	Accept(Key::Colon);
	
	Type* type = AcceptType();
	for (auto& name : names)
	{
		function->AddParameter(name, type, byRef ? ByReference : ByValue);
	}
}

void Compiler::ParseBlockStatements(const set<Key>& followers)
{
	ParseCompoundStatement(followers);
}

void Compiler::ParseCompoundStatement(const set<Key>& followers)
{
	Accept(Key::Begin);

	//auto statement_starters = { Key::If, Key::While, Key::Identifier, Key::Semicolon };
	set<Key> statement_starters = { };
	auto statement_followers = unite(followers, { Key::Semicolon, Key::End });

	do
	{
		bool wasError = false;

		try
		{
			ParseStatement(statement_followers);
		}
		catch (const syntax_error&) { wasError = true; }
		catch (const semantic_error&) { wasError = true; }

		if (wasError)
			SkipTo(statement_followers);
	} while (TryAccept(Key::Semicolon));

	if (!TryAccept(Key::End))
	{
		RaiseSymbolError(ExpectedSemicolon, true);
	}
}

void Compiler::ParseStatement(const set<Key>& followers)
{
	ParseUnlabeledStatement(followers);
}

void Compiler::ParseUnlabeledStatement(const set<Key>& followers)
{
	switch (m_currentSymbol->GetKeyword())
	{
	case Key::If:
	case Key::While:
	case Key::Begin:
		return ParseComplexStatement(followers);
	default:
		return ParseSimpleStatement(followers);
	}
}

void Compiler::ParseComplexStatement(const set<Key>& followers)
{
	if (TryAccept(Key::If, false))
		ParseIfStatement(followers);
	else if (TryAccept(Key::While, false))
		ParseWhileStatement(followers);
	else if (TryAccept(Key::Begin, false))
	{
		ParseCompoundStatement(followers);
	}
	else
	{
		throw exception("Unknown complex statement");
	}
}

void Compiler::ParseIfStatement(const set<Key>& followers)
{
	Accept(Key::If);

	Symbol firstPremiseSymbol = Symbol(*m_currentSymbol);

	Type* premiseType = ParseReturn(&Compiler::ParseExpression, EmptyStarters, followers + Key::Then).ResultType;

	auto before = m_generator.GetBytes();
	m_generator.ClearBytes();

	if (premiseType && !premiseType->Is<BooleanType>())
	{
		InternalRaiseError(BooleanExpected, &firstPremiseSymbol);
	}

	Accept(Key::Then);

	if (TryAccept(Key::Begin, false))
		Parse(&Compiler::ParseCompoundStatement, EmptyStarters, followers + Key::Else);
	else
		Parse(&Compiler::ParseStatement, EmptyStarters, followers + Key::Else);

	auto then = m_generator.GetBytes();
	m_generator.ClearBytes();

	if (TryAccept(Key::Else))
	{
		if (TryAccept(Key::Begin, false))
			Parse(&Compiler::ParseCompoundStatement, EmptyStarters, followers);
		else
			Parse(&Compiler::ParseStatement, EmptyStarters, followers);

		auto elsePart = m_generator.GetBytes();
		m_generator.ClearBytes();

		m_generator.SetBytes(before);
		m_generator.PutJumpIfZero(then.size() + JUMP_SIZE);
		m_generator.PutRange(then);
		m_generator.PutJump(elsePart.size());
		m_generator.PutRange(elsePart);
	}
	else
	{
		m_generator.SetBytes(before);
		m_generator.PutJumpIfZero(then.size());
		m_generator.PutRange(then);
	}	
}

void Compiler::ParseWhileStatement(const set<Key>& followers)
{
	Accept(Key::While);
	auto before = m_generator.GetBytes();
	m_generator.ClearBytes();

	Symbol firstPremiseSymbol = Symbol(*m_currentSymbol);

	Type* premiseType = ParseReturn(&Compiler::ParseExpression, EmptyStarters, followers + Key::Do).ResultType;

	if (premiseType && !premiseType->Is<BooleanType>())
	{
		InternalRaiseError(BooleanExpected, &firstPremiseSymbol);
	}

	auto premise = m_generator.GetBytes();
	m_generator.ClearBytes();

	Accept(Key::Do);

	if (TryAccept(Key::Begin, false))
		Parse(&Compiler::ParseCompoundStatement, EmptyStarters, followers);
	else
		Parse(&Compiler::ParseStatement, EmptyStarters, followers);

	auto body = m_generator.GetBytes();
	m_generator.ClearBytes();

	m_generator.SetBytes(before);
	m_generator.PutRange(premise);
	m_generator.PutJumpIfZero(body.size() + JUMP_SIZE);
	m_generator.PutRange(body);
	m_generator.PutJump(-1 * (body.size() + JUMP_SIZE * 2 + premise.size()));
}

void Compiler::ParseSimpleStatement(const set<Key>& followers)
{
	Identifier* ident = GetCurrentIdentifier();

	if (ident && (ident->Is<Variable>() || ident && ident->Is<Function>()))
	{
		ParseAssignmentStatement(followers);
	}
	else if (ident && ident->Is<Procedure>())
	{
		return ParseProcedureCall();
	}
	else if (TryAccept(Key::Identifier, false))
	{
		ErrorCode code = GetCurrentIdentifier() ? InvalidIdentifierUse : IdentifierNotDefined;
		RaiseSymbolError(code);

		m_scope->AddIdentifier(new Variable(m_currentSymbol->GetString(), nullptr));
		ParseAssignmentStatement(followers);
	}
	else
	{
		ParseEmptyStatement(followers);
	}
}

void Compiler::ParseEmptyStatement(const set<Key>& /*followers*/)
{
	// ¯\_(ツ)_/¯
}

ExpressionResult Compiler::ParseExpression()
{
	ExpressionResult result = ParseSimpleExpression();
	Type* prev_type = result.ResultType;

	switch (m_currentSymbol->GetKeyword())
	{
	case Key::Equal:
	case Key::NotEqual:
	case Key::Less:
	case Key::LessOrEqual:
	case Key::More:
	case Key::MoreOrEqual:
	{
		result.IsVariable = false;

		Symbol operation = Symbol(*m_currentSymbol);
		NextSymbol();

		Type* next_type = ParseSimpleExpression().ResultType;

		if (prev_type && next_type)
		{
			prev_type = prev_type->OperationAllowed(operation.GetKeyword(), next_type);
			if (!prev_type)
				InternalRaiseError(IncompatibleTypes, &operation, true);
		}

		m_generator.PutOperation(operation.GetKeyword());
	}
	default:
		break;
	}

	result.ResultType = prev_type;

	return result;
}

ExpressionResult Compiler::ParseSimpleExpression()
{
	bool modifier = TryAccept(Key::Add) || TryAccept(Key::Subtract);
	bool minus = m_acceptedSymbol->GetKeyword() == Key::Subtract;

	Symbol possibleModifier = Symbol(*m_acceptedSymbol);

	ExpressionResult result = ParseAddend();
	Type* prevType = result.ResultType;

	if (modifier && prevType && !prevType->Is<IntType>() && !prevType->Is<RealType>())
	{
		InternalRaiseError(TypeError, &possibleModifier, true);
	}

	while (
		TryAccept(Key::Add)
		|| TryAccept(Key::Subtract)
		|| TryAccept(Key::Or))
	{
		result.IsVariable = false;

		Symbol operation = Symbol(*m_acceptedSymbol);

		Type* nextType = ParseAddend().ResultType;

		if (prevType && nextType)
		{
			prevType = prevType->OperationAllowed(operation.GetKeyword(), nextType);

			if (!prevType)
				InternalRaiseError(IncompatibleTypes, &operation, true);
		}

		m_generator.PutOperation(operation.GetKeyword());
	}

	if (modifier && minus)
	{
		m_generator.PutByte(g::ByteCode::UnaryNegation);
		result.IsVariable = false;
	}

	result.ResultType = prevType;

	return result;
}

ExpressionResult Compiler::ParseAddend()
{
	ExpressionResult result = ParseMultiplier();
	Type* prevType = result.ResultType;

	while (
		TryAccept(Key::Multiply)
		|| TryAccept(Key::Divide)
		|| TryAccept(Key::Div)
		|| TryAccept(Key::Mod)
		|| TryAccept(Key::And))
	{
		result.IsVariable = false;

		Symbol operation = Symbol(*m_acceptedSymbol);

		Type* nextType = ParseMultiplier().ResultType;

		if (prevType && nextType)
		{
			prevType = prevType->OperationAllowed(operation.GetKeyword(), nextType);
			if (!prevType)
				InternalRaiseError(IncompatibleTypes, &operation, true);
		}

		m_generator.PutOperation(operation.GetKeyword());
	}

	result.ResultType = prevType;

	return result;
}

ExpressionResult Compiler::ParseMultiplier()
{
	ExpressionResult result;
	result.IsVariable = false;

	if (TryAccept(Key::OpenRoundBracket))
	{
		result.ResultType = ParseExpression().ResultType;
		Accept(Key::CloseRoundBracket);
	}
	else if (IsCurrentSymbolFunctionName())
	{
		result.ResultType = ParseFunctionCall();
	}
	else if (IsCurrentSymbolVariableName())
	{
		result.ResultType = ParseVariable();
		result.IsVariable = true;
	}
	else if (TryAccept(Key::Not))
	{
		Symbol notSymbol = Symbol(*m_acceptedSymbol);

		Type* type = ParseMultiplier().ResultType;
		result.ResultType = type;

		if (!type->Is<BooleanType>())
		{
			InternalRaiseError(TypeError, &notSymbol, true);
		}

		m_generator.PutOperation(Key::Not);
	}
	else if (TryAccept(Key::Constant, false) || IsCurrentSymbolConstName())
	{
		result.ResultType = ParseUnsignedConstant();
	}
	else if (TryAccept(Key::Identifier))
	{
		auto ident = m_scope->FindIdentifier(m_acceptedSymbol->GetString());
		if (auto enumVal = ident->As<EnumValue>())
		{
			result.ResultType = enumVal->GetType();

			if (enumVal->GetType()->Is<BooleanType>())
			{
				bool boolValue = enumVal->GetName() == "true";
				Value value;
				value.SetValue(boolValue);

				m_generator.PushConstant(&value);
			}
		}
		else
		{
			RaiseAcceptedSymbolError(IdentifierNotDefined);
			m_scope->AddIdentifier(new Variable(m_acceptedSymbol->GetString(), nullptr));
		}
	}
	else
	{
		RaiseSymbolError(ForbiddenSymbol, true);
	}

	result.ResultType = Type::GetUnderlyingType(result.ResultType);
	return result;
}

Type* Compiler::ParseFunctionCall()
{
	m_generator.SetEnabled(false);

	Accept(Key::Identifier);
	Function* function = GetAcceptedIdentifier()->As<Function>();

	ParseParametersCall(function);

	return function->GetType();
}

void Compiler::ParseParametersCall(Callable * callable)
{
	Accept(Key::OpenRoundBracket);

	auto paramters = callable->GetParameters();
	size_t i = 0;

	do
	{
		if (i >= paramters.size())
			RaiseSymbolError(ParametersNotMatch, true);

		ParseActualParameter(paramters[i++]);
	} while (TryAccept(Key::Comma));

	if (i != paramters.size())
		RaiseSymbolError(ParametersNotMatch, true);

	Accept(Key::CloseRoundBracket);
}

void Compiler::ParseActualParameter(Parameter* formalParameter)
{
	ExpressionResult result = ParseExpression();
	Type* formalType = formalParameter->GetType();

	if (formalType && !formalType->OperationAllowed(Key::Assign, result.ResultType))
	{
		RaiseAcceptedSymbolError(ActualParameterNotMatch);
	}

	if (formalParameter->GetPassingMode() == ByReference && !result.IsVariable)
	{
		RaiseAcceptedSymbolError(ReferenceExpected);
	}
}

Type* Compiler::ParseUnsignedConstant()
{
	Identifier* ident = GetCurrentIdentifier();

	if (auto constant = ident->As<Constant>())
	{
		NextSymbol();
		m_generator.SetEnabled(false);
		return constant->GetType();
	}
	else
	{
		Accept(Key::Constant);

		ValueSymbol* val_symbol = dynamic_cast<ValueSymbol*>(m_acceptedSymbol.get());
		m_generator.PushConstant(&val_symbol->Get());

		return GetConstantType(val_symbol);
	}
}

void Compiler::ParseAssignmentStatement(const set<Key>& followers)
{
	Type* type = nullptr;
	Identifier* ident = GetCurrentIdentifier();

	if (ident && ident->Is<Variable>())
	{
		type = ParseVariable();
	}
	else
	{
		Accept(Key::Identifier);
		Function* func = m_scope->FindIdentifier(m_acceptedSymbol->GetString())->As<Function>();

		if (!func && func != m_scope->GetScopeFunction())
		{
			RaiseAcceptedSymbolError(InvalidIdentifierUse, true);
		}
		else
		{
			type = func->GetType();
		}
	}
	
	Accept(Key::Assign);
	Symbol assign = Symbol(*m_acceptedSymbol);

	Type* exprType = ParseExpression().ResultType;

	if (type && exprType && !type->OperationAllowed(Key::Assign, exprType))
	{
		InternalRaiseError(IncompatibleTypes, &assign);
	}

	m_generator.PutOperation(Key::Assign);
}

void Compiler::ParseProcedureCall()
{
	Accept(Key::Identifier);
	Procedure* proc = GetAcceptedIdentifier()->As<Procedure>();

	bool writeln = proc->GetName() == "writeln";
	if (!writeln)
		m_generator.SetEnabled(false);

	ParseParametersCall(proc);

	if (writeln)
		m_generator.PutByte(g::ByteCode::Writeln);
}

Type* Compiler::ParseVariable()
{
	Accept(Key::Identifier);
	
	string variableName = m_acceptedSymbol->GetString();
	Variable* var = m_scope->FindIdentifier(variableName)->As<Variable>();
	Type* type = var ? var->GetType() : nullptr;

	if (!var)
	{
		RaiseAcceptedSymbolError(InvalidIdentifierUse);
	}

	while (TryAccept(Key::Caret)) {
		if (!type) continue;

		if (auto refType = type->As<ReferenceType>()) {
			type = refType->GetBaseType();
		}
		else
		{
			type = nullptr;
			RaiseAcceptedSymbolError(TypeError, true);
		}
	}

	if (!var->GetType()->Is<ReferenceType>())
	{
		m_generator.PushVariable(var);
	}
	else
	{
		m_generator.SetEnabled(false);
	}

	return type;
}

bool Compiler::TryAccept(Key expectedSymbol, bool getNext)
{
	return Accept(expectedSymbol, getNext, false);
}

bool Compiler::Accept(Key expectedSymbol, bool getNext, bool report)
{
	if (m_currentSymbol->GetKeyword() == expectedSymbol)
	{
		if (getNext)
		{
			// передача владения над старым символом
			m_acceptedSymbol = std::move(m_currentSymbol);
			NextSymbol();
		}
			
		return true;
	}
	else
	{
		if (m_currentSymbol->GetKeyword() == Key::EndOfFile) {
			throw unexpected_eof();
		}

		if (report)
		{
			RaiseSymbolError(GetCodeFor(expectedSymbol));
			throw syntax_error();
		}
			
		return false;
	}
}

void Compiler::InternalRaiseError(ErrorCode errorCode, Symbol * symbol, bool semantic_critical /*= false*/)
{
	m_generator.SetEnabled(false);

	m_lexer->RegisterError(errorCode, "", symbol->GetLinePosition(), symbol->GetLine());
	if (semantic_critical)
		throw semantic_error();
}

void Compiler::RaiseSymbolError(ErrorCode errorCode, bool semantic_critical /*= false*/)
{
	InternalRaiseError(errorCode, m_currentSymbol.get(), semantic_critical);
}

void Compiler::RaiseAcceptedSymbolError(ErrorCode errorCode, bool semantic_critical /*= false*/)
{
	InternalRaiseError(errorCode, m_acceptedSymbol.get(), semantic_critical);
}

void Compiler::SkipTo(set<Key> keys)
{
	while (m_currentSymbol->GetKeyword() != Key::EndOfFile && keys.find(m_currentSymbol->GetKeyword()) == keys.end())
		NextSymbol();

	if (m_currentSymbol->GetKeyword() == Key::EndOfFile)
		m_skippedToEnd = true;
}

Identifier * Compiler::GetCurrentIdentifier(bool recursive /*= true*/)
{
	if (m_currentSymbol->GetKeyword() != Key::Identifier) return nullptr;

	return m_scope->FindIdentifier(m_currentSymbol->GetString(), recursive);
}

Identifier * Compiler::GetAcceptedIdentifier(bool recursive /*= true*/)
{
	if (m_acceptedSymbol->GetKeyword() != Key::Identifier) return nullptr;

	return m_scope->FindIdentifier(m_acceptedSymbol->GetString(), recursive);
}

bool Compiler::IsCurrentSymbolVariableName()
{
	Identifier* ident = GetCurrentIdentifier();

	if (ident == nullptr)
		return false;
	else
		return ident->Is<Variable>();
}

bool Compiler::IsCurrentSymbolFunctionName()
{
	Identifier* ident = GetCurrentIdentifier();

	if (ident == nullptr)
		return false;
	else
		return ident->Is<Function>();
}

bool Compiler::IsCurrentSymbolConstName()
{
	Identifier* ident = GetCurrentIdentifier();

	if (ident == nullptr)
		return false;
	else
		return ident->Is<Constant>();
}

void Compiler::InitializeBaseScope()
{
	m_scope = make_unique<Scope>();

	m_scope->AddIdentifier(new IntType());
	m_scope->AddIdentifier(new RealType());
	m_scope->AddIdentifier(new CharType());
	m_scope->AddIdentifier(new StringType());
	m_scope->AddIdentifier(new BooleanType());

	auto writeln = make_unique<Procedure>("writeln");
	writeln->AddParameter("arg", nullptr, ByValue);

	m_scope->AddIdentifier(writeln.release());
}

void Compiler::OpenScope(Function* function /*= nullptr*/)
{
	m_scope = make_unique<Scope>(m_scope.release(), function);

	if (function != nullptr)
	{
		auto params = function->GetParameters();

		for (auto param : params)
		{
			m_scope->AddIdentifier(new Parameter(*param));
		}
	}
}

void Compiler::CloseScope()
{
	Scope* parentScope = m_scope->GetParentScope();
	m_scope.reset(parentScope);
}

void Compiler::CloseAllScopes()
{
	while (m_scope.get())
		CloseScope();
}

Type* Compiler::AcceptType()
{
	Accept(Key::Identifier);
	auto* ident = GetAcceptedIdentifier();

	if (ident == nullptr)
	{
		RaiseAcceptedSymbolError(IdentifierNotDefined);
		return nullptr;
	}
	else if (!ident->Is<Type>())
	{
		RaiseAcceptedSymbolError(InvalidIdentifierUse);
		return nullptr;
	}
	else
		return ident->As<Type>();
}

Type * Compiler::GetConstantType(ValueSymbol * constantSymbol)
{
	Value constantValue = constantSymbol->Get();

	switch (constantValue.GetType())
	{
	case ValueType::Int:
		return Scope::GetBaseType<IntType>();
	case ValueType::Double:
		return Scope::GetBaseType<RealType>();
	case ValueType::String:
		return Scope::GetBaseType<StringType>();
	case ValueType::Char:
		return Scope::GetBaseType<CharType>();
	default:
		return nullptr;
	}
}
