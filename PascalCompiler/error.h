#pragma once

#include <map>
#include "symbol.h"

enum ErrorCode : int
{
	ExpectedIdentifier = 2,
	ExpectedProgram = 3,
	// ...
	ForbiddenSymbol = 6,
	// ...
	TypeError = 10,
	// ...
	ExpectedEnd = 13,
	ExpectedSemicolon = 14,
	// ...
	InvalidConstant = 50,
	// ...
	ExpectedThen = 52,
	// ...
	ExpectedDo = 54,
	// ...
	ExpectedDot = 61,
	// ...
	InvalidCharConstant = 75,
	CharConstantTooLong = 76,
	// ...
	CommentUnclosed = 86,
	// ...
	InvalidIdentifierUse = 100,
	DuplicateDefinition = 101,
	// ...
	IdentifierNotDefined = 104,
	// ...
	FunctionTypeMissing = 123,
	// ...
	ParametersNotMatch = 126,
	// ...
	BooleanExpected = 135,
	// ...
	ReferenceExpected = 141,
	// ...
	IncompatibleTypes = 182,
	// ...
	ActualParameterNotMatch = 200,
	RealConstantDigitExpected = 201,
	// ...
	IntegerTooLarge = 203,
	// ...
	FeatureUnsupported = 900,
	UnexpectedEOF = 901
};

struct CompileError
{
	ErrorCode error_code;

	size_t line_number;
	size_t line_position;

	std::string extra_info;

	std::string ToString();
	void Print(ostream& stream);

	static std::map<int, std::string> error_map;
};

ErrorCode GetCodeFor(Key keyword);