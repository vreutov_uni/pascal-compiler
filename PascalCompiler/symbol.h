#pragma once

#include <string>
#include <map>
#include <variant>

#include "value.h"

using namespace std;

enum class Key
{
	// non keywords
	None = 0,
	Identifier,
	Constant,
	EndOfFile,

	// keywords
	Program,
	Label,
	Const,
	Type,
	Packed,
	Array,
	Of,
	Record,
	End,
	Case,
	Set,
	Var,
	Procedure,
	Function,
	In,
	Or,
	Div,
	Mod,
	And,
	Not,
	Nil,
	Goto,
	Begin,
	If,
	Then,
	Else,
	While,
	Repeat,
	Until,
	For,
	Do,
	To,
	Downto,
	With,

	// special symbols
	Add,
	Subtract,
	Divide,
	Multiply,
	Dot,
	Comma,
	Colon,
	Semicolon,
	Caret,
	Quote,
	DoubleQuote,
	OpenSquareBracket,
	CloseSquareBraket,
	OpenRoundBracket,
	CloseRoundBracket,
	DoubleDot,
	Equal,
	More,
	Less,
	NotEqual,
	LessOrEqual,
	MoreOrEqual,
	Assign,
	OneLineComment
};

typedef map<string, Key> KeywordMap;

class Symbol
{
	static KeywordMap m_keyword_map;
public:
	Symbol();
	virtual ~Symbol();

	void SetKeyword(Key value);
	Key GetKeyword() const;

	void SetString(string value);
	virtual string GetString() const;

	void SetPosition(size_t lineNumber, size_t linePosition);
	size_t GetLine() const;
	size_t GetLinePosition() const;

	static KeywordMap& GetKeywordMap();

protected:
	string m_string;
	Key m_keyword;

	size_t m_line_number;
	size_t m_line_pos;
};

class ValueSymbol : public Symbol
{
public:
	ValueSymbol();
	virtual ~ValueSymbol();

	virtual string GetString() const override;

	Value& Get();

private:
	Value m_value;
};
