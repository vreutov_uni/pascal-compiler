#include "stdafx.h"
#include "lexer.h"

#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <string>
#include <regex>

Lexer::Lexer() :
	m_max_int(32767)
{
	m_out_stream = &std::cout;
}

Lexer::~Lexer()
{
}

void Lexer::SetInputStream(istream* stream)
{
	m_stream = stream;

	ResetState();

	GetNextChar();
}

void Lexer::ResetState()
{
	m_line_number = -1;
	m_line_pos = -1;

	m_current_char = '\n';
	m_current_line = "";

	m_lines.clear();
	m_error_list.clear();
}

void Lexer::SetOutputStream(ostream * stream)
{
	m_out_stream = stream;
}

unique_ptr<Symbol> Lexer::GetNextSymbol()
{
	auto s = make_unique<Symbol>();
	s->SetPosition(m_line_number, m_line_pos);

	if (m_current_char >= '0' && m_current_char <= '9')
	{
		ParseNumberConstant(s);
	}		
	else if (IsCurrentCharPartOfIdentifier(true))
	{
		ParseKeywordOrIdentifier(s);
	}
	else if (m_current_char == '\'') {
		ParseCharConstant(s);
	}
	else
	{
		switch (m_current_char)
		{
		case '\0':
			s->SetKeyword(Key::EndOfFile);
			break;
		case '\n':
			GetNextChar();
			return GetNextSymbol();
		case ' ':
		case '\t':
			SkipSpaces();
			return GetNextSymbol();
		case '{':
			if (!SkipUntil('}')) {
				RegisterError(CommentUnclosed);
			}
			return GetNextSymbol();
		case '.':
		case '<':
		case '>':
		case ':':
		case '/':
		{
			char first_char = m_current_char;
			string str = string(1, first_char) + GetNextChar();

			Key two_chars_keyword = Symbol::GetKeywordMap()[str];
			if (two_chars_keyword != Key::None)
			{
				s->SetKeyword(two_chars_keyword);
				GetNextChar();
			}
			else
			{
				s->SetKeyword(Symbol::GetKeywordMap()[string(1, first_char)]);
			}

			break;
		}
		default:
			Key one_char_keyword = Symbol::GetKeywordMap()[string(1, m_current_char)];

			if (one_char_keyword != Key::None)
			{
				s->SetKeyword(one_char_keyword);
				GetNextChar();
			}
			else
			{
				RegisterError(ForbiddenSymbol, string(1, m_current_char));
				GetNextChar();
				return GetNextSymbol();
			}

			break;
		}

		if (s->GetKeyword() == Key::OneLineComment)
		{
			if (!SkipUntil('\n'))
			{
				RegisterError(CommentUnclosed);
			}
			return GetNextSymbol();
		}
	}

	return s;
}

void Lexer::Print(const string & str)
{
	*m_out_stream << "[out] " << str << endl;
}

void Lexer::RegisterError(ErrorCode code, string extra_info, size_t position, size_t line)
{
	CompileError error;
	error.error_code = code;
	error.line_number = line != -1 ? line : m_line_number;
	error.line_position = position != -1 ? position : m_line_pos;
	error.extra_info = extra_info;

	m_error_list.push_back(error);
}

bool Lexer::HasErrors()
{
	return m_error_list.size() != 0;
}

void Lexer::ListErrors()
{
	*m_out_stream << m_error_list.size() << " compilation errors: " << endl;

	size_t i_error = 0;

	for (size_t i = 0; i < m_lines.size(); i++)
	{
		*m_out_stream << m_lines[i] << endl;
		
		while (i_error < m_error_list.size() && m_error_list[i_error].line_number == i) {
			m_error_list[i_error++].Print(*m_out_stream);
			*m_out_stream << endl;
		}
	}

	while (i_error < m_error_list.size()) {
		m_error_list[i_error++].Print(*m_out_stream);
		*m_out_stream << endl;
	}
}

char Lexer::GetNextChar()
{
	if (m_current_char != '\0') // ���� �� ��������� ����� ������ �����
	{
		if (m_current_char == '\n') // ���� ��������� ����� ������
		{
			if (getline(*m_stream, m_current_line))
			{
				++m_line_number;
				m_line_pos = -1;

				// ���������� ������ ��������� �� �������
				m_current_line = std::regex_replace(m_current_line, std::regex("\\t"), "    ");

				m_lines.push_back(m_current_line);

				// ���������� ������ \n ��� ����������� ����� ������� ������
				m_current_line += '\n';
			}
			else
			{
				// ���������� ������ \0 ��� ����������� ����� ������ �����
				m_current_char = '\0';
				return m_current_char;
			}
		}
		
		m_current_char = m_current_line[++m_line_pos];
	}

	return m_current_char;
}

bool Lexer::SkipUntil(char ch, bool skip_char)
{
	while (m_current_char != ch && !IsAtEnd())
		GetNextChar();
	
	if (IsAtEnd())
		return false;
	else if (skip_char)
		GetNextChar();

	return true;
}

void Lexer::SkipSpaces()
{
	while (m_current_char == ' ' || m_current_char == '\t')
		GetNextChar();
}

void Lexer::ParseKeywordOrIdentifier(unique_ptr<Symbol>& symbol)
{
	string str = string(1, m_current_char);
	GetNextChar();
	
	while (IsCurrentCharPartOfIdentifier())
	{
		str += m_current_char;
		GetNextChar();
	}

	transform(str.begin(), str.end(), str.begin(), tolower);

	Key keyword = Symbol::GetKeywordMap()[str];
	if (keyword == Key::None)
	{
		symbol->SetKeyword(Key::Identifier);
		symbol->SetString(str);
	}
	else
	{
		symbol->SetKeyword(keyword);
	}
}

void Lexer::ParseCharConstant(unique_ptr<Symbol>& symbol)
{
	string str = "";
	GetNextChar();

	bool ok = false;

	while (m_current_char != '\n')
	{
		if (m_current_char == '\'') {
			if (GetNextChar() != '\'' && str.length() > 0) {
				ok = true;
				break;
			}
		}

		if (m_current_char != '\n')
		{
			str += m_current_char;
			GetNextChar();
		}
	}

	if (!ok)
	{
		if (str.length() > 1)
		{
			RegisterError(InvalidConstant, "", m_line_pos);
		}
		else
		{
			RegisterError(InvalidCharConstant, "", m_line_pos);
		}
	}

	auto valueSymbol = make_unique<ValueSymbol>();
	valueSymbol->SetString(str);
	valueSymbol->SetPosition(symbol->GetLine(), symbol->GetLinePosition());

	if (str.length() <= 1)
		valueSymbol->Get().SetValue(str[0]);
	else
		valueSymbol->Get().SetValue(str);

	symbol = move(valueSymbol);
}

void Lexer::ParseNumberConstant(unique_ptr<Symbol>& symbol)
{
	int number = ParseInt();
	bool is_real = false;

	double real_number = number;

	for (;;)
	{
		if (m_current_char == '.')
		{
			double degree = 0.1;

			GetNextChar();

			if (IsCurrentCharDigit())
			{
				while (IsCurrentCharDigit())
				{
					real_number += degree * (m_current_char - '0');
					degree /= 10;
					GetNextChar();
				}

				is_real = true;
			}
			else
			{
				RegisterError(RealConstantDigitExpected);
				break;
			}
		}

		if (m_current_char == 'e' || m_current_char == 'E')
		{
			GetNextChar();

			int sign = 1;
			if (m_current_char == '-' || m_current_char == '+')
			{
				sign = m_current_char == '-' ? -1 : 1;
				GetNextChar();
			}

			int e_degree = ParseInt();

			if (e_degree == -1)
			{
				RegisterError(RealConstantDigitExpected);
				break;
			}
			else
			{
				real_number *= pow(10.0, sign * e_degree);
				is_real = true;
			}
		}

		break;
	}

	auto valueSymbol = make_unique<ValueSymbol>();
	valueSymbol->SetPosition(symbol->GetLine(), symbol->GetLinePosition());

	if (is_real)
	{
		valueSymbol->Get().SetValue(real_number);
	}
	else
	{
		if (number > m_max_int)
		{
			RegisterError(IntegerTooLarge, "", symbol->GetLine());
			number = m_max_int;
		}

		valueSymbol->Get().SetValue(number);
	}

	symbol = move(valueSymbol);
}

int Lexer::ParseInt()
{
	int number = 0;

	if (!IsCurrentCharDigit())
		return -1;

	while (IsCurrentCharDigit())
	{
		number = number * 10 + (m_current_char - '0');
		GetNextChar();
	}

	return number;
}

bool Lexer::IsCurrentCharPartOfIdentifier(bool is_first)
{
	bool isLetter = (m_current_char >= 'a' && m_current_char <= 'z')
		|| (m_current_char >= 'A' && m_current_char <= 'Z');

	return isLetter || (!is_first && IsCurrentCharDigit()) || m_current_char == '_';
}

bool Lexer::IsCurrentCharDigit()
{
	return m_current_char >= '0' && m_current_char <= '9';
}

bool Lexer::IsAtEnd()
{
	return m_current_char == '\0';
}
