#include "stdafx.h"
#include "generator.h"

namespace g {
	std::vector<byte> Generator::GetBytes()
	{
		return m_bytes;
	}

	void Generator::ClearBytes()
	{
		m_bytes.clear();
	}

	void Generator::SetBytes(const std::vector<byte>& value)
	{
		m_bytes = value;
	}

	bool Generator::GetEnabled()
	{
		return m_enabled;
	}

	void Generator::SetEnabled(bool value)
	{
		m_enabled = value;
	}

	void Generator::PutByte(const byte byte)
	{
		if (!GetEnabled()) return;

		m_bytes.push_back(byte);
	}

	void Generator::PutByte(const ByteCode bytecode)
	{
		PutByte(static_cast<byte>(bytecode));
	}

	void Generator::PutRange(const std::vector<byte>& bytes)
	{
		if (!GetEnabled()) return;

		for (auto& byte : bytes)
			m_bytes.push_back(byte);
	}

	void Generator::PushConstant(Value * value)
	{
		PutByte(ByteCode::Push);
		PutByte(ByteCode::Constant);

		switch (value->GetType())
		{
		case ValueType::Int:
			PutByte(ByteCode::Integer);
			return PutValue(value->GetValue<int>());
		case ValueType::Double:
			PutByte(ByteCode::Real);
			return PutValue(value->GetValue<double>());
		case ValueType::Char:
			PutByte(ByteCode::Char);
			return PutValue(value->GetValue<char>());
		case ValueType::String:
			PutByte(ByteCode::String);
			return PutValue(value->GetValue<string>());
		case ValueType::Boolean:
			PutByte(ByteCode::Boolean);
			return PutValue(value->GetValue<bool>());
		default:
			break;
		}
	}

void Generator::PushVariable(Variable * variable)
{
	if (!GetEnabled()) return;

	PutByte(ByteCode::Push);
	PutByte(ByteCode::Variable);

	if (m_variables.find(variable) == m_variables.end())
	{
		m_variables[variable] = static_cast<byte>(m_variables.size());
		PutByte(m_variables[variable]);
		PutByte(GetCodeFor(variable->GetType()));
	}
	else
	{
		PutByte(m_variables[variable]);
	}
}

	void Generator::PutOperation(Key operation)
	{
		PutByte(GetCodeFor(operation));
	}

	void Generator::PutJump(int offset)
	{
		PutByte(ByteCode::Jump);
		PutValue(offset);
	}

	void Generator::PutJumpIfZero(int offset)
	{
		PutByte(ByteCode::JumpIfZero);
		PutValue(offset);
	}

	ByteCode Generator::GetCodeFor(Type * type)
	{
		while (auto alias = type->As<AliasType>())
			type = alias->GetBaseType();

		if (type->Is<IntType>())
			return ByteCode::Integer;
		if (type->Is<RealType>())
			return ByteCode::Real;
		if (type->Is<CharType>())
			return ByteCode::Char;
		if (type->Is<StringType>())
			return ByteCode::String;
		if (type->Is<BooleanType>())
			return ByteCode::Boolean;

		throw exception((string("unknown type: ") + type->GetName()).c_str());
	}

	ByteCode Generator::GetCodeFor(Key key)
	{
		switch (key)
		{
		case Key::Or:
			return ByteCode::Or;
		case Key::Div:
			return ByteCode::Div;
		case Key::Mod:
			return ByteCode::Mod;
		case Key::And:
			return ByteCode::And;
		case Key::Not:
			return ByteCode::Not;
		case Key::Add:
			return ByteCode::Add;
		case Key::Subtract:
			return ByteCode::Substract;
		case Key::Divide:
			return ByteCode::Divide;
		case Key::Multiply:
			return ByteCode::Multiply;
		case Key::Equal:
			return ByteCode::Equal;
		case Key::More:
			return ByteCode::More;
		case Key::Less:
			return ByteCode::Less;
		case Key::NotEqual:
			return ByteCode::NotEqual;
		case Key::LessOrEqual:
			return ByteCode::LessOrEqual;
		case Key::MoreOrEqual:
			return ByteCode::MoreOrEqual;
		case Key::Assign:
			return ByteCode::Assign;
		default:
			break;
		}

		throw exception("unknown operation");
	}
}
