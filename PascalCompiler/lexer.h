#pragma once
#include "interfaces.h"

#include <vector>

class Lexer :
	public ILexer
{
public:
	Lexer();
	~Lexer();

	// ILexer
	virtual void SetInputStream(istream* stream) override;
	virtual void SetOutputStream(ostream* stream) override;

	virtual unique_ptr<Symbol> GetNextSymbol() override;

	virtual void Print(const string& str) override;

	virtual void RegisterError(ErrorCode code, string extra_info = "", size_t position = -1, size_t line = -1) override;
	virtual bool HasErrors() override;
	virtual void ListErrors() override;

private:
	istream * m_stream;
	ostream * m_out_stream;

	vector<string> m_lines;
	string m_current_line;

	size_t m_current_pos;
	size_t m_line_number;
	size_t m_line_pos;

	char m_current_char;
	const int m_max_int;

	vector<CompileError> m_error_list;

	void ResetState();

	char GetNextChar();
	bool SkipUntil(char ch, bool skip_char = true);
	void SkipSpaces();

	void ParseKeywordOrIdentifier(unique_ptr<Symbol>& symbol);
	void ParseCharConstant(unique_ptr<Symbol>& symbol);
	void ParseNumberConstant(unique_ptr<Symbol>& symbol);
	int ParseInt();

	bool IsCurrentCharPartOfIdentifier(bool is_first = false);
	bool IsCurrentCharDigit();
	bool IsAtEnd();
};

