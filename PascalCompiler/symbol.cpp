#include "stdafx.h"
#include "symbol.h"

#include <algorithm>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <type_traits>
#include <variant>
#include <vector>


KeywordMap Symbol::m_keyword_map = {
	{ "in", Key::In },
	{ "or", Key::Or },
	{ "of", Key::Of },
	{ "to", Key::To },
	{ "do", Key::Do },
	{ "if", Key::If },
	{ "div", Key::Div },
	{ "set", Key::Set },
	{ "and", Key::And },
	{ "for", Key::For },
	{ "not", Key::Not },
	{ "mod", Key::Mod },
	{ "end", Key::End },
	{ "nil", Key::Nil },
	{ "var", Key::Var },
	{ "then", Key::Then },
	{ "case", Key::Case },
	{ "goto", Key::Goto },
	{ "else", Key::Else },
	{ "type", Key::Type },
	{ "with", Key::With },
	{ "label", Key::Label },
	{ "const", Key::Const },
	{ "while", Key::While },
	{ "begin", Key::Begin },
	{ "until", Key::Until },
	{ "array", Key::Array },
	{ "downto", Key::Downto },
	{ "record", Key::Record },
	{ "packed", Key::Packed },
	{ "repeat", Key::Repeat },
	{ "program", Key::Program },
	{ "function", Key::Function },
	{ "procedure", Key::Procedure },

	// one char symbols
	{ "+", Key::Add },
	{ "-", Key::Subtract },
	{ "/", Key::Divide },
	{ "*", Key::Multiply },
	{ ".", Key::Dot },
	{ ",", Key::Comma },
	{ ":", Key::Colon },
	{ ";", Key::Semicolon },
	{ "^", Key::Caret },
	{ "'", Key::Quote },
	{ "\"", Key::DoubleQuote },
	{ "[", Key::OpenSquareBracket },
	{ "]", Key::CloseSquareBraket },
	{ "(", Key::OpenRoundBracket },
	{ ")", Key::CloseRoundBracket },
	{ "=", Key::Equal },
	{ ">", Key::More },
	{ "<", Key::Less },

	// two char symbols
	{ "..", Key::DoubleDot },
	{ "<>", Key::NotEqual },
	{ "<=", Key::LessOrEqual },
	{ ">=", Key::MoreOrEqual },
	{ ":=", Key::Assign },
	{ "//", Key::OneLineComment }
};

Symbol::Symbol() :
	m_string("")
{
}

Symbol::~Symbol()
{
}

void Symbol::SetKeyword(Key value)
{
	m_keyword = value;
}

Key Symbol::GetKeyword() const
{
	return m_keyword;
}

void Symbol::SetString(string value)
{
	m_string = value;
}

string Symbol::GetString() const
{
	if (m_string == "" && GetKeyword() != Key::Identifier)
	{
		auto keyword = GetKeyword();
		auto result = std::find_if(m_keyword_map.begin(), m_keyword_map.end(), [&](const KeywordMap::value_type &pair)
		{
			return pair.second == keyword;
		});

		if (result != m_keyword_map.end())
			const_cast<string&>(m_string) = (*result).first;
	}
	return m_string;
}

void Symbol::SetPosition(size_t line_number, size_t line_position)
{
	m_line_number = line_number;
	m_line_pos = line_position;
}

size_t Symbol::GetLine() const
{
	return m_line_number;
}

size_t Symbol::GetLinePosition() const
{
	return m_line_pos;
}

//std::string Symbol::ToString() const
//{
//	string type;
//
//	switch (m_type)
//	{
//	case SymbolType::Keyword: type = "Keyword"; break;
//	case SymbolType::Constant: type = "Constant"; break;
//	case SymbolType::Identifier: type = "Identifier"; break;
//	}
//
//	return type + ": " + GetStringRepresentation();
//}

KeywordMap& Symbol::GetKeywordMap() 
{
	return m_keyword_map;
}

ValueSymbol::ValueSymbol()
{
	SetKeyword(Key::Constant);
}

ValueSymbol::~ValueSymbol()
{
}

string ValueSymbol::GetString() const
{
	return m_string != "" ? m_string : m_value.ToString();
}

Value & ValueSymbol::Get()
{
	return m_value;
}
