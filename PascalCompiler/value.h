#pragma once
#include <variant>
#include <sstream>

enum class ValueType {
	Int,
	Double,
	Char,
	String,
	Boolean
};

class Value
{
	ValueType m_type;
	std::variant<int, double, char, std::string, bool> m_value;

public:
	ValueType GetType() const;

	template<typename T>
	void SetValue(T value);

	template<typename T>
	T GetValue() const;

	std::string ToString() const;

	void Convert(ValueType value);

private:
	template<typename T>
	void AssertType() const;
};

template<typename T>
inline void Value::AssertType() const
{
	static_assert(
		std::is_same<T, double>::value
		|| std::is_same<T, int>::value
		|| std::is_same<T, char>::value
		|| std::is_same<T, bool>::value
		|| std::is_same<T, std::string>::value, "Incorrect type");
}


template<typename T>
inline void Value::SetValue(T value)
{
	AssertType<T>();

	m_value = value;

	if (std::holds_alternative<int>(m_value)) m_type = ValueType::Int;
	else if (std::holds_alternative<char>(m_value)) m_type = ValueType::Char;
	else if (std::holds_alternative<bool>(m_value)) m_type = ValueType::Boolean;
	else if (std::holds_alternative<double>(m_value)) m_type = ValueType::Double;
	else if (std::holds_alternative<std::string>(m_value)) m_type = ValueType::String;
}

template<typename T>
inline T Value::GetValue() const
{
	AssertType<T>();

	return std::get<T>(m_value);
}

inline ValueType Value::GetType() const
{
	return m_type;
}

inline std::string Value::ToString() const
{
	std::stringstream ss;
	ss << std::boolalpha;

	std::visit([&ss](auto&& arg) { ss << arg; }, m_value);

	return ss.str();
}

inline void Value::Convert(ValueType value)
{
	if (GetType() == ValueType::Int && value == ValueType::Double)
		SetValue(static_cast<double>(GetValue<int>()));
	else if (GetType() == ValueType::Char && value == ValueType::String)
		SetValue(std::string(1, GetValue<char>()));
	else
		throw std::exception("invalid type conversion");
}
