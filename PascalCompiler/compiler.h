#pragma once
#include "interfaces.h"
#include <memory>
#include <set>

#include "syntax.h"
#include "generator.h"

enum Starters
{
	EmptyStarters,
	BlockStarters
};

const int JUMP_SIZE = 5;

class Compiler :
	public ICompiler
{
public:
	Compiler(std::shared_ptr<ILexer> lexer);
	virtual ~Compiler();

	virtual void Compile(const std::string& program) override;
	virtual void Compile(istream& program_stream) override;

	virtual bool GetBytes(ostream& output) override;

private:
	static map<Starters, set<Key>> m_starters_map;

	shared_ptr<ILexer> m_lexer;
	unique_ptr<Symbol> m_currentSymbol;
	unique_ptr<Symbol> m_acceptedSymbol;
	unique_ptr<Scope> m_scope;

	g::Generator m_generator;

	Symbol* NextSymbol();
	bool m_skippedToEnd;

	void Parse(void (Compiler::* callable)(const set<Key>&), Starters starters_map_key, const set<Key>& followers, bool raiseError = true, ErrorCode errorCode = ForbiddenSymbol);
	void Parse(void (Compiler::* callable)(const set<Key>&), const set<Key>& starters, const set<Key>& followers, bool raiseError = true, ErrorCode errorCode = ForbiddenSymbol);

	bool CheckStarters(const std::set<Key> & starters, bool raiseError, ErrorCode errorCode, const std::set<Key> & followers);
	bool CheckFollowers(const std::set<Key> & followers, bool wasError);

	template<typename T>
	T ParseReturn(T (Compiler::*callable)(), Starters starters_map_key, const set<Key>& followers, bool raiseError = true, ErrorCode errorCode = ForbiddenSymbol);

	template<typename T>
	T ParseReturn(T (Compiler::*callable)(), const set<Key>& starters, const set<Key>& followers, bool raiseError = true, ErrorCode errorCode = ForbiddenSymbol);

	void Program(const set<Key>& followers);
	void Block(const set<Key>& followers);

	void BlockTypes(const set<Key>& followers);
	void TypeDefinition(const set<Key>& followers);
	Type* ParseType();

	void BlockContants(const set<Key>& followers);
	void ConstantDefinition(const set<Key>& followers);
	Type* ParseConstant();

	void BlockVariables(const set<Key>& followers);
	void SameTypeVariablesDefinition(const set<Key>& followers);

	void BlockFunctions(const set<Key>& followers);
	void FunctionDefinition(const set<Key>& followers);
	Function* ParseFunctionHeader();
	void ParseFormalParametersSection(Function* function);

	void ParseBlockStatements(const set<Key>& followers);
	void ParseCompoundStatement(const set<Key>& followers);
	void ParseStatement(const set<Key>& followers);
	void ParseUnlabeledStatement(const set<Key>& followers);

	void ParseComplexStatement(const set<Key>& followers);
	void ParseIfStatement(const set<Key>& followers);
	void ParseWhileStatement(const set<Key>& followers);

	void ParseSimpleStatement(const set<Key>& followers);
	void ParseEmptyStatement(const set<Key>& followers);
	void ParseAssignmentStatement(const set<Key>& followers);
	void ParseProcedureCall();
	Type* ParseVariable();

	ExpressionResult ParseExpression();
	ExpressionResult ParseSimpleExpression();
	ExpressionResult ParseAddend();
	ExpressionResult ParseMultiplier();
	Type* ParseFunctionCall();
	void ParseParametersCall(Callable * callable);
	void ParseActualParameter(Parameter* formalParameter);
	Type* ParseUnsignedConstant();

	bool TryAccept(Key expectedSymbol, bool getNext = true);
	bool Accept(Key expectedSymbol, bool getNext = true, bool report = true);

	void InternalRaiseError(ErrorCode errorCode, Symbol* symbol, bool semantic_critical = false);

	void RaiseSymbolError(ErrorCode errorCode, bool semantic_critical = false);
	void RaiseAcceptedSymbolError(ErrorCode errorCode, bool semantic_critical = false);

	void SkipTo(set<Key> keys);

	// Semantics
	Identifier* GetCurrentIdentifier(bool recursive = true);
	Identifier* GetAcceptedIdentifier(bool recursive = true);

	bool IsCurrentSymbolVariableName();
	bool IsCurrentSymbolFunctionName();
	bool IsCurrentSymbolConstName();
	
	void InitializeBaseScope();

	void OpenScope(Function* function = nullptr);
	void CloseScope();
	void CloseAllScopes();

	Type* AcceptType();
	Type* GetConstantType(ValueSymbol* constantSymbol);
};

template<typename T>
set<T> operator+(const set<T> one, const set<T> other)
{
	return unite(one, other);
}

template<typename T>
set<T> operator+(const set<T> one, T item)
{
	return unite(one, item);
}

template<typename T>
set<T> unite(const set<T> one, const set<T> other)
{
	auto res = set<T>(one);
	res.insert(other.begin(), other.end());
	return res;
}

template<typename T>
set<T> unite(const set<T> one, T item)
{
	auto res = set<T>(one);
	res.insert(item);
	return res;
}

//
//template<typename T>
//inline void Compiler::Parse(T callable, const set<Key>& starters, const set<Key>& followers, bool raiseError, ErrorCode errorCode)
//{
//	if (!CheckStarters(starters, raiseError, errorCode, followers) return;
//
//	if (starters.size() == 0 || starters.find(m_currentSymbol->GetKeyword()) != starters.end())
//	{
//		bool wasError = false;
//
//		try
//		{
//			(this->*callable)(followers);
//		}
//		catch (const syntax_error&) { wasError = true; }
//		catch (const semantic_error&) { wasError = true; }
//
//		if (!CheckFollowers(followers, wasError)) return;
//	}
//}

template<typename T>
inline T Compiler::ParseReturn(T (Compiler::* callable)(), Starters starters_map_key, const set<Key>& followers, bool raiseError, ErrorCode errorCode)
{
	return ParseReturn(callable, m_starters_map[starters_map_key], followers, errorCode);
}

template<typename T>
inline T Compiler::ParseReturn(T (Compiler::* callable)(), const set<Key>& starters, const set<Key>& followers, bool raiseError, ErrorCode errorCode)
{
	T result;

	if (!CheckStarters(starters, raiseError, errorCode, followers)) return result;

	if (starters.size() == 0 || starters.find(m_currentSymbol->GetKeyword()) != starters.end())
	{
		bool wasError = false;

		try
		{
			result = (this->*callable)();
		}
		catch (const syntax_error&) { wasError = true; }
		catch (const semantic_error&) { wasError = true; }

		if (!CheckFollowers(followers, wasError)) return result;
	}

	return result;
}
