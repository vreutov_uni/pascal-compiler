#pragma once
#include <ostream>
#include <vector>

#include "bytecodes.h"
#include "syntax.h"

namespace g {
	class Generator
	{
	private:
		std::map<Variable*, byte> m_variables;
		std::vector<byte> m_bytes;
		bool m_enabled;

	public:
		std::vector<byte> GetBytes();
		void ClearBytes();
		void SetBytes(const std::vector<byte>& value);

		bool GetEnabled();
		void SetEnabled(bool value);

		void PutByte(const byte byte);
		void PutByte(const ByteCode bytecode);
		void PutRange(const std::vector<byte>& bytes);

		void PushConstant(Value* value);
		void PushVariable(Variable* variable);

		void PutOperation(Key operation);
		void PutJump(int offset);
		void PutJumpIfZero(int offset);

	private:
		template<typename T>
		void PutValue(T value);

		ByteCode GetCodeFor(Type* type);
		ByteCode GetCodeFor(Key key);
	};

	template<typename T>
	inline void Generator::PutValue(T value)
	{
		// �������������� �������� � ����� ����� ������
		byte* raw_bytes = reinterpret_cast<byte*>(&value);

		// ��������� ���������� ����,
		// ������� ��������� �� �������� �������� ���� � 
		int size = sizeof(T);

		// ������ ���������� ����
		std::vector<byte> bytes(raw_bytes, raw_bytes + size);
		PutRange(bytes);
	}

	template<>
	inline void Generator::PutValue(string value)
	{
		// �������������� �������� � ����� ����� ������
		const byte* raw_bytes = reinterpret_cast<const byte*>(value.c_str());

		// ���������� ����,
		// ������� ��������� �� �������� ������ ��������� ����� ������ + 1 

		// ������ ���������� ����
		std::vector<byte> bytes(raw_bytes, raw_bytes + value.length() + 1);
		PutRange(bytes);
	}
}
