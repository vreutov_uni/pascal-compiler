#include "stdafx.h"
#include "syntax.h"

Identifier::Identifier(string name) :
	m_name(name)
{
}

string Identifier::GetName() const { return m_name; }

Scope* Scope::m_baseScope = nullptr;

Scope::Scope(Scope* parentScope /*= nullptr*/, Function* parentFunction /*= nullptr*/) :
	m_parentScope(parentScope), m_scopeFunction(parentFunction)
{
	if (m_parentScope == nullptr)
		m_baseScope = this;
}

Scope * Scope::GetParentScope() const { return m_parentScope; }

Function * Scope::GetScopeFunction() const
{
	return m_scopeFunction;
}

Identifier * Scope::FindIdentifier(string name, bool recursive /*= true*/) const
{
	auto res = m_identifiers.find(name);

	if (res != m_identifiers.end())
		return res->second.get();
	else
	{
		if (m_parentScope != nullptr && recursive) {
			return m_parentScope->FindIdentifier(name);
		}
		else
			return nullptr;
	}
}

void Scope::AddIdentifier(Identifier * ident)
{
	if (ident->GetName().length() == 0)
	{
		m_anonymIdentifiers.push_back(unique_ptr<Identifier>(ident));
		return;
	}

	m_identifiers[ident->GetName()] = unique_ptr<Identifier>(ident);

	if (auto* enum_type = ident->As<EnumType>())
	{
		auto values = enum_type->GetValues();
		for (auto value : values)
		{
			m_identifiers[value->GetName()] = unique_ptr<EnumValue>(value);
		}
	}
}

Scope * Scope::GetBaseScope()
{
	return m_baseScope;
}

BooleanType * Scope::GetBooleanType() const
{
	return GetBaseScope()->FindIdentifier("boolean")->As<BooleanType>();
}

Variable::Variable(string name, Type * type) :
	Identifier(name),
	m_type(type)
{
}

Type * Variable::GetType() { return m_type; }

void EnumType::AddValue(string name) { m_values.push_back(new EnumValue(name, this)); }

vector<EnumValue*> EnumType::GetValues() { return m_values; }

Type * EnumType::OperationAllowed(Key operation, Type * operand)
{
	operand = GetUnderlyingType(operand);

	if (operand == this)
	{
		if (IsCompareOperation(operation))
		{
			return Scope::GetBaseScope()->GetBooleanType();
		}
		else if (operation == Key::Assign
			|| operation == Key::And
			|| operation == Key::Or)
		{
			return this;
		}
	}

	return nullptr;
}

EnumValue::EnumValue(string name, EnumType * type) :
	Identifier(name), m_type(type)
{
}

EnumType * EnumValue::GetType() { return m_type; }

LinkedType::LinkedType(string name, Type * base_type) : Type(name), m_base_type(base_type)
{
}

Type * LinkedType::GetBaseType() { return m_base_type; }

AliasType::AliasType(string name, Type * base_type) : LinkedType(name, base_type)
{
}

Type * AliasType::OperationAllowed(Key operation, Type * operand)
{
	return GetBaseType()->OperationAllowed(operation, operand);
}

ReferenceType::ReferenceType(string name, Type * base_type)
	: LinkedType(name, base_type)
{
}

ReferenceType::ReferenceType(Type * base_type)
	: ReferenceType("", base_type)
{
}

Type * ReferenceType::OperationAllowed(Key operation, Type * operand)
{
	return nullptr;
}

Constant::Constant(string name, Type * type) :
	Variable(name, type)
{
}

Parameter::Parameter(string name, Type * type, PassingMode mode) :
	Variable(name, type),
	m_passingMode(mode)
{
}

Callable::Callable(string name)
	: Identifier(name)
{
}

void Callable::AddParameter(string name, Type * type, PassingMode mode)
{
	m_parameters.push_back(make_unique<Parameter>(name, type, mode));
}

vector<Parameter*> Callable::GetParameters()
{
	vector<Parameter*> params;

	for (auto& param_ptr : m_parameters)
		params.push_back(param_ptr.get());

	return params;
}

Function::Function(string name) :
	Callable(name)
{
}

void Function::SetType(Type * type) { m_type = type; }

Type * Function::GetType() { return m_type; }

RealType::RealType() :
	ScalarType("real")
{
}

Type* RealType::OperationAllowed(Key operation, Type * operand)
{
	// ������� "�������" ��� ������� ��������
	operand = GetUnderlyingType(operand);

	if (operand->Is<RealType>() || operand->Is<IntType>())
	{
		if (IsArithmeticOperation(operation)
			|| operation == Key::Assign)
		{
			return this;
		}
		else if (IsCompareOperation(operation))
		{
			// ����������� �������� ���������� ���
			// ��� ����� �������� �� ��������� �������
			return Scope::GetBaseScope()->GetBooleanType();
		}
	}

	return nullptr;
}

IntType::IntType() :
	ScalarType("integer")
{
}

Type * IntType::OperationAllowed(Key operation, Type * operand)
{
	operand = GetUnderlyingType(operand);

	if (operand->Is<RealType>() || operand->Is<IntType>())
	{
		if (IsArithmeticOperation(operation))
		{
			return operand;
		}
		else if (IsEqualOperation(operation))
		{
			if (operand->Is<IntType>())
				return Scope::GetBaseScope()->GetBooleanType();
		}
		else if (IsCompareOperation(operation))
		{
			return Scope::GetBaseScope()->GetBooleanType();
		}
		else if (operation == Key::Assign && operand->Is<IntType>())
		{
			return this;
		}
		else if (operation == Key::Mod || operation == Key::Div)
		{
			return operand->As<IntType>();
		}
	}

	return nullptr;
}

StringType::StringType() :
	ScalarType("string")
{
}

Type * StringType::OperationAllowed(Key operation, Type * operand)
{
	operand = GetUnderlyingType(operand);

	if (operand->Is<StringType>() || operand->Is<CharType>())
	{
		if (IsEqualOperation(operation))
		{
			return Scope::GetBaseScope()->GetBooleanType();
		}
		else if (operation == Key::Assign || operation == Key::Add)
		{
			return this;
		}
	}

	return nullptr;
}

CharType::CharType() :
	ScalarType("char")
{
}

Type * CharType::OperationAllowed(Key operation, Type * operand)
{
	operand = GetUnderlyingType(operand);

	if (operation == Key::Add) {
		if (operand->Is<CharType>() || operand->Is<StringType>())
			return operand;
	}

	if (operand->Is<CharType>())
	{
		if (IsEqualOperation(operation))
		{
			return Scope::GetBaseScope()->GetBooleanType();
		}
		else if (operation == Key::Assign)
		{
			return this;
		}
	}

	return nullptr;
}

Type::Type(string name) :
	Identifier(name)
{
}

bool Type::IsEqualOperation(Key operation)
{
	return operation == Key::Equal
		|| operation == Key::NotEqual;
}

bool Type::IsCompareOperation(Key operation)
{
	return IsEqualOperation(operation)
		|| operation == Key::More
		|| operation == Key::Less
		|| operation == Key::MoreOrEqual
		|| operation == Key::LessOrEqual;
}

bool Type::IsArithmeticOperation(Key operation)
{
	return operation == Key::Add
		|| operation == Key::Subtract
		|| operation == Key::Divide
		|| operation == Key::Multiply;
}

Type * Type::GetUnderlyingType(Type * type)
{
	while (auto refType = type->As<AliasType>())
		type = refType->GetBaseType();

	return type;
}

ScalarType::ScalarType(string name) :
	Type(name)
{
}

Procedure::Procedure(string name)
	: Callable(name)
{
}

ExpressionResult::ExpressionResult()
	: ResultType(nullptr), IsVariable(false)
{
}
