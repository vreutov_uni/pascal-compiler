#pragma once

#include <string>
#include <memory>
using namespace std;

#include "error.h"

class ICompiler
{
public:
	virtual void Compile(const string& program) = 0;
	virtual void Compile(istream& program_stream) = 0;

	virtual bool GetBytes(ostream& output) = 0;
};

class ILexer
{
public:
	virtual void SetInputStream(istream* stream) = 0;
	virtual void SetOutputStream(ostream* stream) = 0;

	virtual unique_ptr<Symbol> GetNextSymbol() = 0;

	virtual void Print(const string& str) = 0;
	
	virtual void RegisterError(ErrorCode code, string extra_info = "", size_t position = -1, size_t line = -1) = 0;
	virtual bool HasErrors() = 0;
	virtual void ListErrors() = 0;
};