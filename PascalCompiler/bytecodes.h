#pragma once


namespace g {
	typedef unsigned char byte;

	enum class ByteCode : byte
	{
		// operations
		Begin,
		End,
		Push,
		Jump,
		JumpIfZero,
		Add,
		Substract,
		Multiply,
		Divide,
		Mod,
		Div,
		Equal,
		Not,
		And,
		Or,
		UnaryNegation,
		NotEqual,
		More,
		Less,
		MoreOrEqual,
		LessOrEqual,
		Assign,
		Writeln,

		// types
		Variable,
		Constant,

		// value types
		Integer,
		Real,
		Char,
		String,
		Boolean
	};
}