#pragma once

#include <vector>
#include <map>
#include <stack>
#include <istream>
#include <memory>

#include "bytecodes.h"
#include "value.h"

using namespace g;

struct StackItem
{
	Value Value;
	bool IsVariable;

	StackItem(bool isVariable);
};

class Interpreter
{
	std::istream* m_input;
	std::vector<byte> m_bytes;
	std::map<byte, std::shared_ptr<StackItem>> m_variables;
	std::stack<std::shared_ptr<StackItem>> m_stack;

	size_t m_bytesPos;

public:
	Interpreter();
	~Interpreter();

	void Run(std::istream* input);
private:
	void Run();
	void Fail();

	byte GetByte(bool getNext = true);
	ByteCode GetCode(bool getNext = true);
	void CheckByte(ByteCode code);

	template<typename T>
	T GetValue();

	void GetItem();
	void GetConstant();
	void GetVariable();

	std::shared_ptr<StackItem> Pop();

	void PerformOperation(ByteCode operation,
		const std::shared_ptr<StackItem>& arg1,
		const std::shared_ptr<StackItem>& arg2);

	template<typename T>
	void PerformTypedOperation(ByteCode operation,
		Value* val1, Value* val2, Value & result);

	template<typename T>
	bool PerformPlusMinusOperation(ByteCode operation, Value* val1, Value* val2, Value& result);

	template<typename T>
	bool PerformMultiplyDivOperation(ByteCode operation, Value* val1, Value* val2, Value& result);

	template<typename T>
	bool PerformEqualityOperation(ByteCode operation, Value* val1, Value* val2, Value& result);

	template<typename T>
	bool PerformMoreLessCompareOperation(ByteCode operation, Value* val1, Value* val2, Value& result);


	template<typename T>
	bool PerformAssignOperation(ByteCode operation, Value* val1, Value* val2);

	bool PerformLogicOperation(ByteCode operation, Value* val1, Value* val2, Value& result);
	bool PerformModDivOperation(ByteCode operation, Value* val1, Value* val2, Value& result);

	void PerformUnaryOperation(ByteCode operation,
		const std::shared_ptr<StackItem>& arg);


};

template<typename T>
inline T Interpreter::GetValue()
{
	int size = sizeof(T);

	auto start = m_bytes.begin() + m_bytesPos;
	auto end = start + size;

	m_bytesPos += size;

	std::vector<byte> bytes(start, end);
	T* value = reinterpret_cast<T*>(&bytes.front());

	return *value;
}


template<>
inline std::string Interpreter::GetValue()
{
	std::string result;

	while (m_bytes[m_bytesPos] != '\0')
		result += (char)m_bytes[m_bytesPos++];

	++m_bytesPos;
	return result;
}

template<typename T>
inline bool Interpreter::PerformEqualityOperation(ByteCode operation, Value * val1, Value * val2, Value & result)
{
	switch (operation)
	{
	case ByteCode::Equal:
		result.SetValue(val1->GetValue<T>() == val2->GetValue<T>());
		break;
	case ByteCode::NotEqual:
		result.SetValue(val1->GetValue<T>() != val2->GetValue<T>());
		break;
	default:
		return false;
	}

	return true;
}

template<typename T>
inline bool Interpreter::PerformMoreLessCompareOperation(ByteCode operation, Value * val1, Value * val2, Value & result)
{
	switch (operation)
	{
	case ByteCode::More:
		result.SetValue(val1->GetValue<T>() > val2->GetValue<T>());
		break;
	case ByteCode::Less:
		result.SetValue(val1->GetValue<T>() < val2->GetValue<T>());
		break;
	case ByteCode::MoreOrEqual:
		result.SetValue(val1->GetValue<T>() >= val2->GetValue<T>());
		break;
	case ByteCode::LessOrEqual:
		result.SetValue(val1->GetValue<T>() <= val2->GetValue<T>());
		break;
	}

	return true;
}

template<typename T>
inline bool Interpreter::PerformAssignOperation(ByteCode operation, Value * val1, Value * val2)
{
	if (operation != ByteCode::Assign)
		return false;
	
	val1->SetValue(val2->GetValue<T>());
	
	return true;
}

template<typename T>
inline void Interpreter::PerformTypedOperation(ByteCode operation,
	Value* val1, Value* val2, Value & result)
{
	Fail();
}

template<typename T>
inline bool Interpreter::PerformPlusMinusOperation(ByteCode operation, Value * val1, Value * val2, Value & result)
{
	switch (operation)
	{
	case ByteCode::Add:
		result.SetValue((T) (val1->GetValue<T>() + val2->GetValue<T>()));
		break;
	case ByteCode::Substract:
		result.SetValue((T) (val1->GetValue<T>() - val2->GetValue<T>()));
		break;
	default:
		return false;
	}

	return true;
}

template<typename T>
inline bool Interpreter::PerformMultiplyDivOperation(ByteCode operation, Value * val1, Value * val2, Value & result)
{
	switch (operation)
	{
	case ByteCode::Multiply:
		result.SetValue(val1->GetValue<T>() * val2->GetValue<T>());
		break;
	case ByteCode::Divide:
		if (val2->GetValue<T>() == 0) {
			throw std::exception("zero division exception");
		}
		result.SetValue(val1->GetValue<T>() / val2->GetValue<T>());
		break;
	default:
		return false;
	}

	return true;
}

template<>
inline void Interpreter::PerformTypedOperation<int>(ByteCode operation,
	Value* val1, Value* val2, Value & result)
{
	bool done =
		PerformPlusMinusOperation<int>(operation, val1, val2, result)
		|| PerformMultiplyDivOperation<int>(operation, val1, val2, result)
		|| PerformModDivOperation(operation, val1, val2, result)
		|| PerformAssignOperation<int>(operation, val1, val2)
		|| PerformEqualityOperation<int>(operation, val1, val2, result)
		|| PerformMoreLessCompareOperation<int>(operation, val1, val2, result);

	if (!done)
		Fail();
}

template<>
inline void Interpreter::PerformTypedOperation<double>(ByteCode operation,
	Value* val1, Value* val2, Value & result)
{
	bool done =
		PerformPlusMinusOperation<double>(operation, val1, val2, result)
		|| PerformMultiplyDivOperation<double>(operation, val1, val2, result)
		|| PerformAssignOperation<double>(operation, val1, val2)
		|| PerformEqualityOperation<double>(operation, val1, val2, result)
		|| PerformMoreLessCompareOperation<double>(operation, val1, val2, result);

	if (!done)
		Fail();
}

template<>
inline void Interpreter::PerformTypedOperation<char>(ByteCode operation,
	Value* val1, Value* val2, Value & result)
{
	bool done =
		PerformPlusMinusOperation<char>(operation, val1, val2, result)
		|| PerformAssignOperation<char>(operation, val1, val2)
		|| PerformEqualityOperation<char>(operation, val1, val2, result)
		|| PerformMoreLessCompareOperation<char>(operation, val1, val2, result);

	if (!done)
		Fail();
}

template<>
inline void Interpreter::PerformTypedOperation<std::string>(ByteCode operation,
	Value* val1, Value* val2, Value & result)
{
	bool done =
		PerformAssignOperation<std::string>(operation, val1, val2)
		|| PerformEqualityOperation<std::string>(operation, val1, val2, result);

	if (!done && operation == ByteCode::Add)
	{
		result.SetValue(val1->GetValue<std::string>() + val2->GetValue<std::string>());
		done = true;
	}

	if (!done)
		Fail();
}

template<>
inline void Interpreter::PerformTypedOperation<bool>(ByteCode operation,
	Value* val1, Value* val2, Value & result)
{
	bool done =
		PerformLogicOperation(operation, val1, val2, result)
		|| PerformAssignOperation<bool>(operation, val1, val2)
		|| PerformEqualityOperation<bool>(operation, val1, val2, result);

	if (!done)
		Fail();
}