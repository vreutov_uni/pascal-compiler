#include "stdafx.h"
#include "interpreter.h"

#include <iostream>
#include <string>

Interpreter::Interpreter()
{
}


Interpreter::~Interpreter()
{
}

void Interpreter::Run(std::istream * input)
{
	byte byte;
	do
	{
		byte = input->get();
		m_bytes.push_back(byte);
	} while (!input->eof());

	m_bytesPos = 0;

	if (m_bytes.size() > 0)
	{
		try
		{
			Run();
		}
		catch (const std::exception& ex)
		{
			std::cout << "Runtime error occured: " << std::endl << ex.what() << std::endl;
		}
	}
	else
		std::cout << "No bytes read" << std::endl;
}

void Interpreter::Run()
{
	CheckByte(ByteCode::Begin);

	while (m_bytesPos < m_bytes.size() && GetCode(false) != ByteCode::End)
	{
		ByteCode code = GetCode();
		switch (code)
		{
		case ByteCode::Push:
			GetItem(); break;
		case ByteCode::Jump:
		{
			int offset = GetValue<int>();
			m_bytesPos += offset;
			break;
		}
		case ByteCode::JumpIfZero:
		{
			int offset = GetValue<int>();

			auto premise = Pop();
			if (premise->Value.GetValue<bool>() == false)
			{
				m_bytesPos += offset;
			}
			break;
		}
		case ByteCode::Add:
		case ByteCode::Substract:
		case ByteCode::Multiply:
		case ByteCode::Divide:
		case ByteCode::Mod:
		case ByteCode::Div:
		case ByteCode::Equal:
		case ByteCode::NotEqual:
		case ByteCode::And:
		case ByteCode::Or:
		case ByteCode::More:
		case ByteCode::Less:
		case ByteCode::MoreOrEqual:
		case ByteCode::LessOrEqual:
		case ByteCode::Assign:
			PerformOperation(code, Pop(), Pop()); break;
		case ByteCode::Not:
		case ByteCode::Writeln:
		case ByteCode::UnaryNegation:
			PerformUnaryOperation(code, Pop()); break;
		default:
			Fail();
		}
	}

	CheckByte(ByteCode::End);
}

void Interpreter::Fail()
{
	throw std::exception("fail");
}

byte Interpreter::GetByte(bool getNext)
{
	byte result = m_bytes[m_bytesPos];
	if (getNext)
		++m_bytesPos;

	return result;
}

ByteCode Interpreter::GetCode(bool getNext)
{
	return static_cast<ByteCode>(GetByte(getNext));
}

void Interpreter::CheckByte(ByteCode code)
{
	if (GetCode() != code)
		Fail();
}

void Interpreter::GetItem()
{
	switch (GetCode())
	{
	case ByteCode::Variable:
		GetVariable(); break;
	case ByteCode::Constant:
		GetConstant(); break;
	default:
		Fail();
	}
}

void Interpreter::GetConstant()
{
	auto item = std::make_shared<StackItem>(false);

	switch (GetCode())
	{
	case ByteCode::Integer:
		item->Value.SetValue(GetValue<int>());
		break;
	case ByteCode::Real:
		item->Value.SetValue(GetValue<double>());
		break;
	case ByteCode::Char:
		item->Value.SetValue(GetValue<char>());
		break;
	case ByteCode::String:
		item->Value.SetValue(GetValue<std::string>());
		break;
	case ByteCode::Boolean:
		item->Value.SetValue(GetValue<bool>());
		break;
	default:
		Fail();
	}

	m_stack.push(item);
}

void Interpreter::GetVariable()
{
	byte index = GetByte();

	auto var = m_variables.find(index);

	if (var != m_variables.end())
	{
		m_stack.push(var->second);
	}
	else
	{
		auto item = std::make_shared<StackItem>(true);

		switch (GetCode())
		{
		case ByteCode::Integer:
			item->Value.SetValue(0);
			break;
		case ByteCode::Real:
			item->Value.SetValue(0.0);
			break;
		case ByteCode::Char:
			item->Value.SetValue('\0');
			break;
		case ByteCode::String:
			item->Value.SetValue(std::string());
			break;
		case ByteCode::Boolean:
			item->Value.SetValue(false);
			break;
		}

		m_variables[index] = item;
		m_stack.push(item);
	}
}

std::shared_ptr<StackItem> Interpreter::Pop()
{
	auto result = m_stack.top();
	m_stack.pop();
	return result;
}

void Interpreter::PerformOperation(ByteCode operation, const std::shared_ptr<StackItem>& arg1, const std::shared_ptr<StackItem>& arg2)
{
	Value tempValue;
	Value* val1 = &arg1->Value;
	Value* val2 = &arg2->Value;

	if (val1->GetType() != val2->GetType())
	{
		ValueType typeFrom;
		ValueType typeTo;

		if (val1->GetType() == ValueType::Int || val2->GetType() == ValueType::Int)
		{
			typeFrom = ValueType::Int;
			typeTo = ValueType::Double;
		}
		else if (val1->GetType() == ValueType::Char || val2->GetType() == ValueType::Char)
		{
			typeFrom = ValueType::Char;
			typeTo = ValueType::String;
		}
		else
			Fail();

		StackItem* itemToConvert = val1->GetType() == typeFrom ? arg1.get() : arg2.get();
		Value*& pointerToConvert = val1->GetType() == typeFrom ? val1 : val2;

		if (itemToConvert->IsVariable)
		{
			tempValue = itemToConvert->Value;
			pointerToConvert = &tempValue;
		}

		pointerToConvert->Convert(typeTo);
	}

	auto item = std::make_shared<StackItem>(false);
	Value& result = item->Value;

	ValueType type = arg1->Value.GetType();
	switch (type)
	{
	case ValueType::Int:
		PerformTypedOperation<int>(operation, val1, val2, result);
		break;
	case ValueType::Double:
		PerformTypedOperation<double>(operation, val1, val2, result);
		break;
	case ValueType::Char:
		PerformTypedOperation<char>(operation, val1, val2, result);
		break;
	case ValueType::String:
		PerformTypedOperation<std::string>(operation, val1, val2, result);
		break;
	case ValueType::Boolean:
		PerformTypedOperation<bool>(operation, val1, val2, result);
		break;
	default:
		break;
	}

	if (operation != ByteCode::Assign) {
		m_stack.push(item);
	}
}

inline bool Interpreter::PerformLogicOperation(ByteCode operation, Value * val1, Value * val2, Value & result)
{
	switch (operation)
	{
	case ByteCode::And:
		result.SetValue(val1->GetValue<bool>() && val2->GetValue<bool>());
		break;
	case ByteCode::Or:
		result.SetValue(val1->GetValue<bool>() || val2->GetValue<bool>());
		break;
	default:
		return false;
	}

	return true;
}

inline bool Interpreter::PerformModDivOperation(ByteCode operation, Value * val1, Value * val2, Value & result)
{
	if (operation == ByteCode::Mod || operation == ByteCode::Div)
		if (val2->GetValue<int>() == 0) {
			throw std::exception("zero division exception");
		}

	switch (operation)
	{
	case ByteCode::Mod:
		result.SetValue(val1->GetValue<int>() % val2->GetValue<int>());
		break;
	case ByteCode::Div:
		result.SetValue(val1->GetValue<int>() / val2->GetValue<int>());
		break;
	default:
		return false;
	}

	return true;
}

void Interpreter::PerformUnaryOperation(ByteCode operation, const std::shared_ptr<StackItem>& arg)
{
	switch (operation)
	{
	case ByteCode::Not:
		arg->Value.SetValue(!arg->Value.GetValue<bool>());
		break;
	case ByteCode::UnaryNegation:
		if (arg->Value.GetType() == ValueType::Int)
			arg->Value.SetValue(-arg->Value.GetValue<int>());
		else
			arg->Value.SetValue(-arg->Value.GetValue<double>());
		break;
	case ByteCode::Writeln:
		std::cout << arg->Value.ToString() << std::endl;
		break;
	default:
		Fail();
		break;
	}
}

StackItem::StackItem(bool isVariable)
	: IsVariable(isVariable)
{
}
