// Interpreter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "interpreter.h"

#include <iostream>
#include <fstream>
#include <string>

void usage(const std::string& program_name)
{
	std::cout << "Usage: " << program_name << " <path to file>" << std::endl;
}

int main(int argc, char *argv[])
{
	std::string program_name = argv[0];

	if (argc < 2) {

		usage(program_name);
		return 1;
	}

	std::string filename = argv[1];
	std::ifstream file(filename, std::ios::in | std::ios::binary);

	Interpreter worker;
	worker.Run(&file);

    return 0;
}

